/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/front/main.scss';


import * as bootstrap from 'bootstrap'
// start the Stimulus application
import './bootstrap';
import './js/pageModal.js';
import './js/startColor.js';
import './js/testShowButton.js';
import './js/carousel.js';


// import './js/test.js';
console.log('app ok');
