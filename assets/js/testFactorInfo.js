// avec une for each
// => je récupère les elements avec la classe accordionItemHeading
let btns = document.querySelectorAll('.btnTestInfo');
// => je rajoute un addEventlistener pour chacun des elements ci-dessus   :
btns.forEach(function(btn) {
    btn.addEventListener('click', toggleAccordion);
})


function toggleAccordion() {
    let isClose = this.parentElement.nextElementSibling.classList.contains('infoClose');
    // Je ferme Tout
    btns.forEach(function(btn) {
        btn.parentElement.nextElementSibling.classList.replace('infoOpen', 'infoClose');
    })
    // J'ouvre le parent de celui sur lequel j'ai cliqué (le fameux this) seulement s'il était fermé à la base (isClose)
    if (isClose) {
        this.parentElement.nextElementSibling.classList.replace('infoClose', 'infoOpen');
    }

}


