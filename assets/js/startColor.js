console.log('startColor.js ok');

let radios = document.querySelectorAll('.form-check-input');

radios.forEach(function(radio){
    radio.addEventListener('click', function(){
        if (radio.checked) {
            radio.parentElement.parentElement.parentElement.classList.replace('alert-danger', 'alert-success');
        }
    });
});

// console.log('modalTest.js  : ok');

// let modalTest = new bootstrap.Modal(document.querySelector('#modalTest'));
// modalTest.toggle();