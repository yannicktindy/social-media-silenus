## start the project
Make the .env.local with bdd access :DATABASE_URL="mysql://root:@127.0.0.1:3306/silenus?serverVersion=8&charset=utf8mb4"
composer install
yarn install
symfony console d:d:c           to create database
symfony console m:mi            to make migrations
symfony console d:m:m           to migrate migrations to database
symfony console hautelook:fixtures:load --purge-with-truncate     to launch fixtures
