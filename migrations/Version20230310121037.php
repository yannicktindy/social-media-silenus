<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230310121037 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(255) NOT NULL, contenu LONGTEXT NOT NULL, content LONGTEXT NOT NULL, updated_at DATETIME NOT NULL, is_active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_category (article_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_53A4EDAA7294869C (article_id), INDEX IDX_53A4EDAA12469DE2 (category_id), PRIMARY KEY(article_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_tag (article_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_919694F97294869C (article_id), INDEX IDX_919694F9BAD26311 (tag_id), PRIMARY KEY(article_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE factor (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, desc_fr LONGTEXT NOT NULL, desc_eng LONGTEXT NOT NULL, INDEX IDX_ED38EC001E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_user (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, is_valid TINYINT(1) NOT NULL, is_first TINYINT(1) NOT NULL, INDEX IDX_7FA93CD7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE imgback (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE imgsquare (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE impact (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, kinkster_id INT DEFAULT NULL, creation DATETIME NOT NULL, impact_slug VARCHAR(255) NOT NULL, gen_level DOUBLE PRECISION NOT NULL, INDEX IDX_C409C0071E5D0459 (test_id), INDEX IDX_C409C00768776098 (kinkster_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kinkster (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(50) NOT NULL, user_slug VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, creation DATETIME NOT NULL, update_at DATETIME NOT NULL, is_fr TINYINT(1) NOT NULL, is_letter TINYINT(1) NOT NULL, is_validated TINYINT(1) NOT NULL, is_suspended TINYINT(1) NOT NULL, is_deleted TINYINT(1) NOT NULL, visit INT DEFAULT NULL, UNIQUE INDEX UNIQ_A66202C8E7927C74 (email), UNIQUE INDEX UNIQ_A66202C8E21CD7AB (user_slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, sender_id INT DEFAULT NULL, room_id INT DEFAULT NULL, parent_message_id INT DEFAULT NULL, subject VARCHAR(255) DEFAULT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, last_answer DATETIME DEFAULT NULL, INDEX IDX_B6BD307FF624B39D (sender_id), INDEX IDX_B6BD307F54177093 (room_id), INDEX IDX_B6BD307F14399779 (parent_message_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, factor_id INT DEFAULT NULL, quest_eng VARCHAR(255) NOT NULL, quest_fr VARCHAR(255) NOT NULL, INDEX IDX_B6F7494E1E5D0459 (test_id), INDEX IDX_B6F7494EBC88C1A3 (factor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(50) NOT NULL, title VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, desc_fr VARCHAR(255) NOT NULL, desc_eng VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room_kinkster (room_id INT NOT NULL, kinkster_id INT NOT NULL, INDEX IDX_684C20C54177093 (room_id), INDEX IDX_684C20C68776098 (kinkster_id), PRIMARY KEY(room_id, kinkster_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE score (id INT AUTO_INCREMENT NOT NULL, impact_id INT DEFAULT NULL, factor_id INT DEFAULT NULL, level DOUBLE PRECISION NOT NULL, INDEX IDX_32993751D128BC9B (impact_id), INDEX IDX_32993751BC88C1A3 (factor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, imgsquare_id INT DEFAULT NULL, imgback_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, test_slug VARCHAR(255) NOT NULL, sub_title VARCHAR(255) NOT NULL, sous_titre VARCHAR(255) NOT NULL, desc_fr LONGTEXT NOT NULL, desc_eng LONGTEXT NOT NULL, updated_at DATETIME NOT NULL, advanced TINYINT(1) NOT NULL, is_old TINYINT(1) NOT NULL, is_active TINYINT(1) NOT NULL, count INT DEFAULT NULL, INDEX IDX_D87F7E0CA493B111 (imgsquare_id), INDEX IDX_D87F7E0CCD4E6813 (imgback_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visit (id INT AUTO_INCREMENT NOT NULL, visit_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article_category ADD CONSTRAINT FK_53A4EDAA7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_category ADD CONSTRAINT FK_53A4EDAA12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_tag ADD CONSTRAINT FK_919694F97294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_tag ADD CONSTRAINT FK_919694F9BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE factor ADD CONSTRAINT FK_ED38EC001E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE image_user ADD CONSTRAINT FK_7FA93CD7A76ED395 FOREIGN KEY (user_id) REFERENCES kinkster (id)');
        $this->addSql('ALTER TABLE impact ADD CONSTRAINT FK_C409C0071E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE impact ADD CONSTRAINT FK_C409C00768776098 FOREIGN KEY (kinkster_id) REFERENCES kinkster (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF624B39D FOREIGN KEY (sender_id) REFERENCES kinkster (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F54177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F14399779 FOREIGN KEY (parent_message_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E1E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494EBC88C1A3 FOREIGN KEY (factor_id) REFERENCES factor (id)');
        $this->addSql('ALTER TABLE room_kinkster ADD CONSTRAINT FK_684C20C54177093 FOREIGN KEY (room_id) REFERENCES room (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE room_kinkster ADD CONSTRAINT FK_684C20C68776098 FOREIGN KEY (kinkster_id) REFERENCES kinkster (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE score ADD CONSTRAINT FK_32993751D128BC9B FOREIGN KEY (impact_id) REFERENCES impact (id)');
        $this->addSql('ALTER TABLE score ADD CONSTRAINT FK_32993751BC88C1A3 FOREIGN KEY (factor_id) REFERENCES factor (id)');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0CA493B111 FOREIGN KEY (imgsquare_id) REFERENCES imgsquare (id)');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0CCD4E6813 FOREIGN KEY (imgback_id) REFERENCES imgback (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_category DROP FOREIGN KEY FK_53A4EDAA7294869C');
        $this->addSql('ALTER TABLE article_category DROP FOREIGN KEY FK_53A4EDAA12469DE2');
        $this->addSql('ALTER TABLE article_tag DROP FOREIGN KEY FK_919694F97294869C');
        $this->addSql('ALTER TABLE article_tag DROP FOREIGN KEY FK_919694F9BAD26311');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE factor DROP FOREIGN KEY FK_ED38EC001E5D0459');
        $this->addSql('ALTER TABLE image_user DROP FOREIGN KEY FK_7FA93CD7A76ED395');
        $this->addSql('ALTER TABLE impact DROP FOREIGN KEY FK_C409C0071E5D0459');
        $this->addSql('ALTER TABLE impact DROP FOREIGN KEY FK_C409C00768776098');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FF624B39D');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F54177093');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F14399779');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E1E5D0459');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494EBC88C1A3');
        $this->addSql('ALTER TABLE room_kinkster DROP FOREIGN KEY FK_684C20C54177093');
        $this->addSql('ALTER TABLE room_kinkster DROP FOREIGN KEY FK_684C20C68776098');
        $this->addSql('ALTER TABLE score DROP FOREIGN KEY FK_32993751D128BC9B');
        $this->addSql('ALTER TABLE score DROP FOREIGN KEY FK_32993751BC88C1A3');
        $this->addSql('ALTER TABLE test DROP FOREIGN KEY FK_D87F7E0CA493B111');
        $this->addSql('ALTER TABLE test DROP FOREIGN KEY FK_D87F7E0CCD4E6813');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_category');
        $this->addSql('DROP TABLE article_tag');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE factor');
        $this->addSql('DROP TABLE image_user');
        $this->addSql('DROP TABLE imgback');
        $this->addSql('DROP TABLE imgsquare');
        $this->addSql('DROP TABLE impact');
        $this->addSql('DROP TABLE kinkster');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE room_kinkster');
        $this->addSql('DROP TABLE score');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE visit');
    }
}
