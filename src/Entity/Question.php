<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuestionRepository::class)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $questEng = null;

    #[ORM\Column(length: 255)]
    private ?string $questFr = null;

    #[ORM\ManyToOne(inversedBy: 'questions')]
    private ?Test $test = null;

    #[ORM\ManyToOne(inversedBy: 'questions', cascade: ['persist'])]
    private ?Factor $factor = null;

    public function __toString(): string
    {
        return (string) $this->questFr;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestEng(): ?string
    {
        return $this->questEng;
    }

    public function setQuestEng(string $questEng): self
    {
        $this->questEng = $questEng;

        return $this;
    }

    public function getQuestFr(): ?string
    {
        return $this->questFr;
    }

    public function setQuestFr(string $questFr): self
    {
        $this->questFr = $questFr;

        return $this;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getFactor(): ?Factor
    {
        return $this->factor;
    }

    public function setFactor(?Factor $factor): self
    {
        $this->factor = $factor;

        return $this;
    }
}
