<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TestRepository::class)]
class Test
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $name = null;
    
    #[ORM\Column(length: 255)]
    private ?string $testSlug = null;
    
    #[ORM\Column(length: 255)]
    private ?string $subTitle = null;

    #[ORM\Column(length: 255)]
    private ?string $sousTitre = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $descFr = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $descEng = null;
    
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    private ?bool $advanced = null;
    
    #[ORM\Column]
    private ?bool $isOld = null;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Question::class, cascade: ['persist', 'remove'])]
    private Collection $questions;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Factor::class, cascade: ['persist', 'remove'])]
    private Collection $factors;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Impact::class, cascade: ['persist', 'remove'])]
    private Collection $impacts;

    #[ORM\ManyToOne(inversedBy: 'tests')]
    private ?Imgsquare $imgsquare = null;

    #[ORM\ManyToOne(inversedBy: 'tests')]
    private ?Imgback $imgback = null;

    #[ORM\Column]
    private ?bool $isActive = null;

    #[ORM\Column(nullable: true)]
    private ?int $count = null;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->factors = new ArrayCollection();
        $this->impacts = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setTest($this);
        }

        return $this;
    }

    public function removeQuestion(question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getTest() === $this) {
                $question->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Factor>
     */
    public function getFactors(): Collection
    {
        return $this->factors;
    }

    public function addFactor(Factor $factor): self
    {
        if (!$this->factors->contains($factor)) {
            $this->factors->add($factor);
            $factor->setTest($this);
        }

        return $this;
    }

    public function removeFactor(Factor $factor): self
    {
        if ($this->factors->removeElement($factor)) {
            // set the owning side to null (unless already changed)
            if ($factor->getTest() === $this) {
                $factor->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Impact>
     */
    public function getImpacts(): Collection
    {
        return $this->impacts;
    }

    public function addImpact(Impact $impact): self
    {
        if (!$this->impacts->contains($impact)) {
            $this->impacts->add($impact);
            $impact->setTest($this);
        }

        return $this;
    }

    public function removeImpact(Impact $impact): self
    {
        if ($this->impacts->removeElement($impact)) {
            // set the owning side to null (unless already changed)
            if ($impact->getTest() === $this) {
                $impact->setTest(null);
            }
        }

        return $this;
    }

    public function getDescFr(): ?string
    {
        return $this->descFr;
    }

    public function setDescFr(string $descFr): self
    {
        $this->descFr = $descFr;

        return $this;
    }

    public function getDescEng(): ?string
    {
        return $this->descEng;
    }

    public function setDescEng(string $descEng): self
    {
        $this->descEng = $descEng;

        return $this;
    }

    public function getTestSlug(): ?string
    {
        return $this->testSlug;
    }

    public function setTestSlug(string $testSlug): self
    {
        $this->testSlug = $testSlug;

        return $this;
    }

    public function isAdvanced(): ?bool
    {
        return $this->advanced;
    }

    public function setAdvanced(bool $advanced): self
    {
        $this->advanced = $advanced;

        return $this;
    }

    public function getImgsquare(): ?Imgsquare
    {
        return $this->imgsquare;
    }

    public function setImgsquare(?Imgsquare $imgsquare): self
    {
        $this->imgsquare = $imgsquare;

        return $this;
    }

    public function getImgback(): ?Imgback
    {
        return $this->imgback;
    }

    public function setImgback(?Imgback $imgback): self
    {
        $this->imgback = $imgback;

        return $this;
    }

    public function getSubTitle(): ?string
    {
        return $this->subTitle;
    }

    public function setSubTitle(string $subTitle): self
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    public function getSousTitre(): ?string
    {
        return $this->sousTitre;
    }

    public function setSousTitre(string $sousTitre): self
    {
        $this->sousTitre = $sousTitre;

        return $this;
    }

    public function isIsOld(): ?bool
    {
        return $this->isOld;
    }

    public function setIsOld(bool $isOld): self
    {
        $this->isOld = $isOld;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }

}
