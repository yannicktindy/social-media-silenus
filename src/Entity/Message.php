<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subject = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $LastAnswer = null;

    #[ORM\ManyToOne(inversedBy: 'messagesSended')]
    private ?Kinkster $sender = null;

    #[ORM\ManyToOne(inversedBy: 'messages')]
    private ?Room $room = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'childMessages', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(name: 'parent_message_id', referencedColumnName: 'id', nullable: true)]
    private $parentMessage;

    #[ORM\OneToMany(mappedBy: 'parentMessage', targetEntity: self::class)]
    private $childMessages;

    public function __construct()
    {
        $this->childMessages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getLastAnswer(): ?\DateTimeInterface
    {
        return $this->LastAnswer;
    }

    public function setLastAnswer(?\DateTimeInterface $LastAnswer): self
    {
        $this->LastAnswer = $LastAnswer;

        return $this;
    }

    public function getSender(): ?Kinkster
    {
        return $this->sender;
    }

    public function setSender(?Kinkster $kinster): self
    {
        $this->sender = $kinster;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getParentMessage(): ?Message
    {
        return $this->parentMessage;
    }

    public function setParentMessage(?Message $parentMessage): self
    {
        $this->parentMessage = $parentMessage;

        return $this;
    }

    public function getChildMessages(): Collection
    {
        return $this->childMessages;
    }

    public function addChildMessage(Message $message): self
    {
        if (!$this->childMessages->contains($message)) {
            $this->childMessages[] = $message;
            $message->setParentMessage($this);
        }

        return $this;
    }

    public function removeChildMessage(Message $message): self
    {
        if ($this->childMessages->contains($message)) {
            $this->childMessages->removeElement($message);
            if ($message->getParentMessage() === $this) {
                $message->setParentMessage(null);
            }
        }

        return $this;
    }
}
