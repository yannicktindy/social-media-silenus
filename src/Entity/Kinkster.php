<?php

namespace App\Entity;

use App\Repository\KinksterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: KinksterRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class Kinkster implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 50)]
    private ?string $pseudo = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $userSlug = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null; 

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $creation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $updateAt = null;

    #[ORM\Column]
    private ?bool $isFr = null;

    #[ORM\Column]
    private ?bool $isLetter = null;

    #[ORM\Column]
    private ?bool $isValidated = null;

    #[ORM\Column]
    private ?bool $isSuspended = null;

    #[ORM\Column]
    private ?bool $isDeleted = null;

    #[ORM\OneToMany(mappedBy: 'kinkster', targetEntity: Impact::class)]
    private Collection $impacts;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ImageUser::class)]
    private Collection $imageUsers;

    #[ORM\Column(nullable: true)]
    private ?int $visit = null;

    #[ORM\ManyToMany(targetEntity: Room::class, mappedBy: 'kinksters')]
    private Collection $rooms;

    #[ORM\OneToMany(mappedBy: 'sender', targetEntity: Message::class)]
    private Collection $messagesSended;


    public function __construct()
    {
        $this->impacts = new ArrayCollection();
        $this->imageUsers = new ArrayCollection();
        $this->rooms = new ArrayCollection();
        $this->messagesSended = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getPseudo();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getCreation(): ?\DateTimeInterface
    {
        return $this->creation;
    }

    public function setCreation(\DateTimeInterface $creation): self
    {
        $this->creation = $creation;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }


    /**
     * @return Collection<int, Impact>
     */
    public function getImpacts(): Collection
    {
        return $this->impacts;
    }

    public function addImpact(Impact $impact): self
    {
        if (!$this->impacts->contains($impact)) {
            $this->impacts->add($impact);
            $impact->setKinkster($this);
        }

        return $this;
    }

    public function removeImpact(Impact $impact): self
    {
        if ($this->impacts->removeElement($impact)) {
            // set the owning side to null (unless already changed)
            if ($impact->getKinkster() === $this) {
                $impact->setKinkster(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ImageUser>
     */
    public function getImageUsers(): Collection
    {
        return $this->imageUsers;
    }

    public function addImageUser(ImageUser $imageUser): self
    {
        if (!$this->imageUsers->contains($imageUser)) {
            $this->imageUsers->add($imageUser);
            $imageUser->setUser($this);
        }

        return $this;
    }

    public function removeImageUser(ImageUser $imageUser): self
    {
        if ($this->imageUsers->removeElement($imageUser)) {
            // set the owning side to null (unless already changed)
            if ($imageUser->getUser() === $this) {
                $imageUser->setUser(null);
            }
        }

        return $this;
    }

    public function isIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function isIsSuspended(): ?bool
    {
        return $this->isSuspended;
    }

    public function setIsSuspended(bool $isSuspended): self
    {
        $this->isSuspended = $isSuspended;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getUserSlug(): ?string
    {
        return $this->userSlug;
    }

    public function setUserSlug(string $userSlug): self
    {
        $this->userSlug = $userSlug;

        return $this;
    }

    public function isIsFr(): ?bool
    {
        return $this->isFr;
    }

    public function setIsFr(bool $isFr): self
    {
        $this->isFr = $isFr;

        return $this;
    }

    public function isIsLetter(): ?bool
    {
        return $this->isLetter;
    }

    public function setIsLetter(bool $isLetter): self
    {
        $this->isLetter = $isLetter;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVisit(): ?int
    {
        return $this->visit;
    }

    public function setVisit(?int $visit): self
    {
        $this->visit = $visit;

        return $this;
    }

    /**
     * @return Collection<int, Room>
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(Room $room): self
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms->add($room);
            $room->addKinkster($this);
        }

        return $this;
    }

    public function removeRoom(Room $room): self
    {
        if ($this->rooms->removeElement($room)) {
            $room->removeKinkster($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessagesSended(): Collection
    {
        return $this->messagesSended;
    }

    public function addMessagesSended(Message $messagesSended): self
    {
        if (!$this->messagesSended->contains($messagesSended)) {
            $this->messagesSended->add($messagesSended);
            $messagesSended->setSender($this);
        }

        return $this;
    }

    public function removeMessagesSended(Message $messagesSended): self
    {
        if ($this->messagesSended->removeElement($messagesSended)) {
            // set the owning side to null (unless already changed)
            if ($messagesSended->getSender() === $this) {
                $messagesSended->setSender(null);
            }
        }

        return $this;
    }



}
