<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // 7 radio buttons
        $builder
            ->add('choices', ChoiceType::class, [
                'choices' => [
                    '1' => 0,
                    '2' => 8,
                    '3' => 16,
                    '4' => 24,
                    '5' => 32,
                    '6' => 40,
                    '7' => 50,
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => false,
                'choice_name' => null,   
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Proposition suivante',
                'attr' => [
                    'class' => 'btn btn-outline-light',
                ],
            ])
        
        ;
    }
}