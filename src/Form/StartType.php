<?php

namespace App\Form;

use App\Entity\Pict;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class StartType extends AbstractType 
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('opt1', RadioType::class, [
            'label' => false,
            'required' => true,
            'attr' => [
                'class' => 'form-check-input',
            ],
        ])
        ->add('opt2', RadioType::class, [
            'label' => false,
            'required' => true,
            'attr' => [
                'class' => 'form-check-input',
            ],
        ])
        ->add('opt3', RadioType::class, [
            'label' => false,
            'required' => true,
            'attr' => [
                'class' => 'form-check-input',
            ],
        ])

        ->add('save', SubmitType::class, [
            'label' => 'Ok !',
            'attr' => [
                'class' => 'btn btn-outline-secondary',
            ],
        ]);
    }
}