<?php

namespace App\Controller\QuickAdmin;

use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdminQuickBoardController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private VisitRepository $visitRepository,
        private KinksterRepository $kinksterRepository,
        private ImpactRepository $impactRepository,
    )
    {
    }


    #[Route('/admin/quick/', name: 'app_admin_quick')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function board(): Response
    {
        $todayVisits = $this->visitRepository->todayVisits();
        $allVisits = $this->visitRepository->findAll();
        $lastSevenDaysVisits = $this->visitRepository->lastSevenDaysVisits(); //ok
        $countEachDayVisitsForLastSevenDays = $this->visitRepository->countEachDayVisitsForLastSevenDays();
        // dump($countEachDayVisitsForLastSevenDays);
        // $weeklyVisits = $this->visitRepository->weeklyVisits(); 
        // dump($weeklyVisits) ; 
        $allImpacts = $this->impactRepository->findAll();
        $allKinksters = $this->kinksterRepository->findAll();






        return $this->render('admin/quick/board.html.twig', [
            'controller_name' => 'AdminQuickBoardController',
            'allVisits' => $allVisits,
            'todayVisits' => $todayVisits,

            'allImapcts' => $allImpacts,
            'allKinksters' => $allKinksters,
        
        ]);
    }
}
