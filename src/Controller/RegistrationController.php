<?php

namespace App\Controller;

use App\Entity\Kinkster;
use App\Form\RegistrationFormType;
use App\Repository\KinksterRepository;
use App\Security\KinksterAuthenticator;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\String\Slugger\SluggerInterface;


class RegistrationController extends AbstractController
{
    public function __construct( 
        private EntityManagerInterface $em,
        private KinksterRepository $kinksterRepository,

    ) {}

    #[Route('/register', name: 'app_register')]
    public function register(
        Request $request, 
        UserPasswordHasherInterface $userPasswordHasher, 
        UserAuthenticatorInterface $userAuthenticator, 
        KinksterAuthenticator $authenticator, 
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger,
        MailerService $mailer,
        SessionInterface $session,
        ): Response
    {
        $lang = $session->get('lang');
        $user = new Kinkster();
        $form = $this->createForm(RegistrationFormType::class, $user);
        // set creation 
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setCreation(new \DateTime('now'));
            $user->setUpdateAt(new \DateTime('now'));
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setRoles(['ROLE_USER']);
            $user->setUserSlug('kinkster-' . uniqid());
            $user->setIsValidated(false);
            $user->setIsSuspended(false);
            $user->setIsDeleted(false);
            if ($lang == 'fr') {
                $user->setIsFr(true);

            } else {
                $user->setIsFr(false);

            }
            $user->setIsLetter(true);
            $user->setVisit(0);

            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            $to = $user->getEmail();
            $subject = 'Inscription sur INSIDIOME';
            $userSlug = $user->getUserSlug();
            $userName = $user->getPseudo();
            $mailer->registrationEmail($to, $subject, $userName, $userSlug);

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'lang' => $lang,
        ]);
    }


}
