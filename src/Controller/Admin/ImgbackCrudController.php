<?php

namespace App\Controller\Admin;

use App\Entity\Imgback;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ImgbackCrudController extends AbstractCrudController
{
    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/img/back';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;
    
    public static function getEntityFqcn(): string
    {
        return Imgback::class;
    }
       
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title')
                ->setLabel('Nom de l\'image')
                ->setRequired(true),
            ImageField::new('name')
                ->setLabel('Sélectionner une image')
                ->setUploadDir($this::TEST_UPLOAD_PATH)
                ->setUploadedFileNamePattern($this::TEST_BASE_PATH.'/'.'[randomhash].[extension]')
                ->setRequired(true)
            ];
    }
}
