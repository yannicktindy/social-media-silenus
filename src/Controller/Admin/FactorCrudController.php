<?php

namespace App\Controller\Admin;

use App\Entity\Factor;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FactorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Factor::class;
    }

    public function configureFields(string $pageName): iterable
    {


        $nom = TextField::new('nom')->setLabel('Nom du facteur');
        $name = TextField::new('name')->setLabel('Name of the factor');
        $descFr = TextEditorField::new('descFr')->setLabel('Description française');
        $descEng = TextEditorField::new('descEng')->setLabel('Description anglaise');
        $test = AssociationField::new('test')->setLabel('Test');
        $questions = AssociationField::new('questions')->setLabel('Questions'); // number of questions
        $testColl = CollectionField::new('test')->setLabel('Test');
        $questionColl = CollectionField::new('questions')->setLabel('Questions')->setTemplatePath('admin/question/questionInTest.html.twig');
        
        if ($pageName === 'index') {
            return [ $nom, $name, $test, $questions
            ];
        } else if ($pageName === 'detail') {
            return  [FormField::addTab('Facteur'), $nom, $name, $descFr, $descEng, $test, $questions, $testColl, $questionColl
            ];
        } else if ($pageName === 'edit') {
            return [ 
                $nom, $name, $descFr, $descEng, 
            ];
        } else {
            return [ $nom, $name, $descFr, $descEng, 
            ];
        }

        return [
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
   
}
