<?php

namespace App\Controller\Admin;

use App\Entity\Test;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


class TestCrudController extends AbstractCrudController
{
    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/test/pict';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;

    public static function getEntityFqcn(): string
    {
        return Test::class;
    }


    public function configureFields(string $pageName): iterable
    {

        // $id =IdField::new('id')->setLabel('id');
        $date = DateField::new('updatedAt'); 
        $name = TextField::new('name')->setLabel('Nom du test');
        $testSlug = TextField::new('testSlug')->setLabel('Slug du test');
        $sousTitre = TextField::new('sousTitre')->setLabel('Sous-titre');
        $subTitle = TextField::new('subTitle')->setLabel('Sub-title');
        $descFr = TextEditorField::new('descFr')->setLabel('Description française');
        $descEng = TextEditorField::new('descEng')->setLabel('Description anglaise');
        $advanced = BooleanField::new('advanced')->setLabel('Test Avancé ?');
        $isOld = BooleanField::new('isOld')->setLabel('Ancien test ?');
        $isActive = BooleanField::new('isActive')->setLabel('Actif ?');
        $imgsquare = AssociationField::new('imgsquare')->setLabel('Image carrée');
        $imgback = AssociationField::new('imgback')->setLabel('Image de fond');
        $Factors = AssociationField::new('factors')->setLabel('Facteurs'); // number of Facteurs
        $questions = AssociationField::new('questions')->setLabel('Questions'); // number of questions
        $impacts = AssociationField::new('impacts')->setLabel('Impacts'); // number of questions
        
        $factorColl = CollectionField::new('factors')
            ->setLabel('Facteurs')
            ->setTemplatePath('admin/factor/factorInTest.html.twig')
            ->setRequired(true)
            ->useEntryCrudForm()
            ->allowDelete(false);
            // ->setEntryIsComplex(true)
        $questionColl = CollectionField::new('questions')
            ->setLabel('Questions')
            ->setTemplatePath('admin/question/questionInTest.html.twig')
            ->setRequired(true)
            ->useEntryCrudForm()
            ->allowDelete(false);
            // ->setEntryIsComplex(true)
        $impactColl = CollectionField::new('impacts')->setLabel('Impacts');

    
        if ($pageName === Action::INDEX) {
            // $image = ImageField::new('image')->setLabel('Image')->setBasePath(self::TEST_BASE_PATH);
            // TODO override avec entity.path
            return [ $date, $name, $sousTitre, $advanced, $imgsquare, $imgback, $isActive , $Factors, $questions, $impacts
            ];
        } else if ($pageName === Action::DETAIL) {
            return  [FormField::addTab('Test'), $date, $name, $testSlug, $sousTitre, $subTitle , $descFr, $descEng, $advanced, $isOld, $isActive, $impacts, $Factors, $questions, FormField::addTab('Facteurs'), $factorColl, FormField::addTab('Questions'), $questionColl, 
            ];
        } else if ($pageName === Action::EDIT) {
            // $imgsquare= AssociationField::new('imgsquare')->setLabel('Image carrée');
            // $imgsquare= AssociationField::new('imgback')->setLabel('Image de fond');
            return [ 
                FormField::addTab('Test'), $date, $name, $imgsquare, $testSlug, $sousTitre, $subTitle , $descFr, $descEng, $imgsquare , $imgback, $isActive,  $advanced, $isOld , FormField::addTab('Facteurs'), $factorColl, FormField::addTab('Questions'), $questionColl,   
            ];
        } else {
            $imgsquare= AssociationField::new('imgsquare')->setLabel('Image');
            $imgsquare= AssociationField::new('imgback')->setLabel('Image de fond');
            return [ 
                 $date, $name, $testSlug, $sousTitre, $subTitle , $descFr, $descEng, $imgsquare, $imgback , $isActive, $advanced, $isOld , $imgsquare        
            ];
        }
 
        return [ 
             $date, $name , $testSlug , $descEng , $descFr , $advanced , $Factors , $questions , $impacts , $isOld ,       
        ];   

    }

}
