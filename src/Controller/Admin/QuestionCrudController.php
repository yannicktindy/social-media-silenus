<?php

namespace App\Controller\Admin;

use App\Entity\Factor;
use App\Entity\Test;
use App\Entity\Question;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class QuestionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Question::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $descFr = TextField::new('questFr')->setLabel('Description française');
        $descEng = TextField::new('questEng')->setLabel('Description anglaise');


        $entity = $this->getContext()->getEntity();
        $targetTest = null;

        if ($entity->getInstance() instanceof Test) {

            $targetTest = $entity->getPrimaryKeyValue();

            //Disable field in Mission global form
            // $missionField = FormField::addRow();

        }else if ($entity->getInstance() instanceof Question && $pageName === Crud::PAGE_EDIT) {
            $targetTest = $entity->getInstance()->getTest()->getId();
        }

        //CREATE FACTOR FIELD
        $factorField = AssociationField
            ::new('factor', 'link question to one factor')
            ->setHelp('save the question before selecting a factor')
            ->setQueryBuilder(
                //FILTER FACTOR BY ACTUAL TEST
                function (QueryBuilder $queryBuilder) use ($targetTest) {
                    $queryBuilder->where('entity.test = ' . $targetTest);
                }
            );

        //Disable field in simple Cost form NEW
        if ($entity->getInstance() instanceof Question && $pageName === Crud::PAGE_NEW) {
            $factorField = FormField::addRow();
        }

        if ($pageName === Crud::PAGE_INDEX) {
            $test = AssociationField::new('test')->setLabel('Test');
            return [  
                $descFr, $descEng, $factorField, $test 
            ];
        } 



        return [ 
            $descFr, $descEng, $factorField
        ];

    }
}
