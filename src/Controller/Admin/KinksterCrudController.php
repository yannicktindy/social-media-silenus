<?php

namespace App\Controller\Admin;

use App\Entity\Kinkster;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class KinksterCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Kinkster::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('pseudo'),
            TextEditorField::new('description'),
            BooleanField::new('isFr'),
            BooleanField::new('isLetter'),
            BooleanField::new('isValidated'),
            BooleanField::new('isSuspended'),
            CollectionField::new('impacts'),
        ];    
    }
    
}
