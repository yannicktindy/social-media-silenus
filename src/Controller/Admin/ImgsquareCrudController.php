<?php

namespace App\Controller\Admin;

use App\Entity\Imgsquare;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ImgsquareCrudController extends AbstractCrudController
{
    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/img/square';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;
    
    public static function getEntityFqcn(): string
    {
        return Imgsquare::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title')
                ->setLabel('Nom de l\'image')
                ->setRequired(true),
            ImageField::new('name')
                ->setLabel('Sélectionner une image')
                ->setUploadDir($this::TEST_UPLOAD_PATH)
                ->setUploadedFileNamePattern($this::TEST_BASE_PATH.'/'.'[randomhash].[extension]')
                ->setRequired(true)
            ];
    }
}
