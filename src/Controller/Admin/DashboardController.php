<?php

namespace App\Controller\Admin;

use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\VisitRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard as HtmlDashboard;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        readonly EntityManagerInterface $em,        
        private VisitRepository $visitRepository,
        private KinksterRepository $kinksterRepository,
        private ImpactRepository $impactRepository, 
    ){}


    #[Route('/admin', name: 'admin')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function index(): Response
    {
        $todayVisits = $this->visitRepository->todayVisits();
        $allVisits = $this->visitRepository->findAll();
        $lastSevenDaysVisits = $this->visitRepository->lastSevenDaysVisits(); //ok
        $countEachDayVisitsForLastSevenDays = $this->visitRepository->countEachDayVisitsForLastSevenDays();
        // dump($countEachDayVisitsForLastSevenDays);
        // $weeklyVisits = $this->visitRepository->weeklyVisits(); 
        // dump($weeklyVisits) ; 
        $allImpacts = $this->impactRepository->findAll();
        $allKinksters = $this->kinksterRepository->findAll();

        return $this->render('admin/index.html.twig', [
            'controller_name' => 'DashboardController',
            'allVisits' => $allVisits,
            'todayVisits' => $todayVisits,

            'allImpacts' => $allImpacts,
            'allKinksters' => $allKinksters,
        
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('INSIDIOME');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Retour à l\'accueil', 'fa fa-home', 'app_home');
        yield MenuItem::linkToDashboard('Panneau d\'administration', 'fa fa-dashboard');

        yield MenuItem::subMenu('Infos INSIDIOME', 'fa-solid fa-temperature-half')->setSubItems([
            MenuItem::linkToCrud('Visit', 'fas fa-eye', Visit::class)
            ->setController(VisitCrudController::class),
        ]);

        yield MenuItem::subMenu('Blog', 'fa-solid fa-cubes')->setSubItems([
            MenuItem::linkToCrud('Article', 'fas fa-eye', Article::class)
                ->setController(ArticleCrudController::class),
            MenuItem::linkToCrud('Categories', 'fas fa-eye', Category::class)
                ->setController(CategoryCrudController::class),
            MenuItem::linkToCrud('Tag', 'fas fa-eye', Tag::class)
                ->setController(TagCrudController::class),    
        ]);

        yield MenuItem::subMenu('Rooms', 'fa-regular fa-comment')->setSubItems([
            MenuItem::linkToCrud('Room', 'fas fa-eye', Room::class)
                ->setController(RoomCrudController::class),
            MenuItem::linkToCrud('Message', 'fas fa-eye', Message::class)
                ->setController(MessageCrudController::class),
          
        ]);
        yield MenuItem::subMenu('Infos Kinkster', 'fas fa-users')->setSubItems([
            MenuItem::linkToCrud('Kinkster', 'fas fa-eye', Kinktser::class)
                ->setController(KinksterCrudController::class),
            MenuItem::linkToCrud('Impact', 'fas fa-eye', Impact::class)
                ->setController(ImpactCrudController::class),
            MenuItem::linkToCrud('Score', 'fas fa-eye', Score::class)
                ->setController(ScoreCrudController::class),    
        ]);

        yield MenuItem::subMenu('Eléments de Test', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Test', 'fas fa-eye', Test::class)
                ->setController(TestCrudController::class),
            MenuItem::linkToCrud('Factor', 'fas fa-eye', Factor::class)
                ->setController(FactorCrudController::class),
            MenuItem::linkToCrud('Question', 'fas fa-eye', Question::class)
                ->setController(QuestionCrudController::class),    
        ]);

        yield MenuItem::subMenu('Gestion des images', 'fa-solid fa-image')->setSubItems([  
            MenuItem::linkToCrud('Image carré', 'fas fa-eye', Imgsquare::class)
                ->setController(ImgsquareCrudController::class),
            MenuItem::linkToCrud('Image de fond', 'fas fa-eye', Imgback::class)
                ->setController(ImgbackCrudController::class),
        ]);
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}