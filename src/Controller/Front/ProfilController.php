<?php

namespace App\Controller\Front;

use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ProfilController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private KinksterRepository $kinksterRepository,
        private ScoreRepository $scoreRepository,
        private ImpactRepository $impactRepository,

    ) {}

    #[Route('/profil/{userSlug}', name: 'app_profil')]
    public function index(
        string $userSlug,
        EntityManagerInterface $entityManager,
    ): Response
    {
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $impacts = $this->impactRepository->findBy(['kinkster' => $kinkster]);
        $countImage = count($kinkster->getImageUsers());
        $profilImage = '';
        $idImage = 0;
        if($countImage > 0){
            $profilImage = $kinkster->getImageUsers()[0]->getName();
            $idImage = $kinkster->getImageUsers()[0]->getId();
        }

        $kinkster->setVisit($kinkster->getVisit() + 1);
        $entityManager->persist($kinkster);
        $entityManager->flush();

        return $this->render('front/profil/profil.html.twig', [
            'controller_name' => 'ProfilController',
            'kinkster' => $kinkster,
            'impacts' => $impacts,
            'profilImage' => $profilImage,
            'idImage' => $idImage,
            'countImage' => $countImage,
        ]);

    }

    #[Route('/profil/activate/{userSlug}', name: 'app_profil_activate')]
    public function activate(
        string $userSlug,
        EntityManagerInterface $entityManager,
        ): Response
    {
        $user = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $user->setIsValidated(true);
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render('/front/profil/activate.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/profil/board/{userSlug}', name: 'app_profil_board')]
    public function board(
        string $userSlug,
        UserInterface $user,
    ): Response
    {
        
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $kinksterIdentifier = $kinkster->getEmail();
        $userIdentifier = $user->getUserIdentifier();
        if ($kinksterIdentifier !== $userIdentifier) {
            return $this->render('front/profil/profil.html.twig');
        }
        $countImage = count($kinkster->getImageUsers());
        $profilImage = '';
        $idImage = 0;
        if($countImage > 0){
            $profilImage= $kinkster->getImageUsers()[0]->getName();
            $idImage = $kinkster->getImageUsers()[0]->getId();
        }

        return $this->render('front/profil/board.html.twig', [
            'controller_name' => 'ProfilController',
            'kinkster' => $kinkster,
            'profilImage' => $profilImage,
            'idImage' => $idImage,
            'countImage' => $countImage,
        ]);    

    }
}
