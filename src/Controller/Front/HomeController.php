<?php

namespace App\Controller\Front;

use Doctrine\ORM\EntityManagerInterface;
use app\Entity\Test;
use App\Repository\TestRepository;
use App\Repository\VisitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em,
        private TestRepository $testRepository
    ) {}

    #[Route('/home', name: 'app_home')]
    public function home(
        VisitRepository $visitRepository,
        SessionInterface $session, 
    ): Response
    {
        $lang = $session->get('lang');
        $nbVisit = $visitRepository->count([]);
        $basicTests = $this->testRepository->findBy(['advanced' => false]);

        return $this->render('front/home/home.html.twig', [
            'controller_name' => 'HomeController',
            'basicTests' => $basicTests,
            'nbVisit' => $nbVisit,
            'lang' => $lang,
        ]);
    }
}
