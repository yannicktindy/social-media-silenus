<?php

namespace App\Controller\Front;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Repository\RoomRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


#[Route('/tchat')]
class TchatController extends AbstractController
{

    #[Route('/', name: 'app_tchat_list')]
    public function list(
        RoomRepository  $roomRepository,
        SessionInterface $session,
    ): Response
    {
        $lang = $session->get('lang');
        return $this->render('front/tchat/list.html.twig', [
            'rooms' => $roomRepository->findAll(),
            'lang' => $lang,
        ]);
    }

    #[Route('/enter/{roomSlug}', name: 'app_tchat_enter')]
    public function enter(
        string $roomSlug,
        RoomRepository  $roomRepository,
        SessionInterface $session,
        EntityManagerInterface $em,
    ): Response
    {   
        $user = $this->getUser();
        if($user == null){
            return $this->redirectToRoute('app_login');
        }

        $room = $roomRepository->findOneBy(['slug' => $roomSlug]);
        $room->addKinkster($user);
        $em->persist($room);
        $em->flush();

        $lang = $session->get('lang');
        return $this->render('front/tchat/enter.html.twig', [
            'room' => $roomRepository->findOneBy(['slug' => $roomSlug]),
            'lang' => $lang,
        ]);
    }

    #[Route('/room/{roomSlug}', name: 'app_tchat_room', methods: ['GET', 'POST'])]
    public function room(
        string $roomSlug,
        RoomRepository  $roomRepository,
        MessageRepository $messageRepository,
        Request $request, 
        SessionInterface $session,
    ): Response
    {
        $lang = $session->get('lang');
        $room = $roomRepository->findOneBy(['slug' => $roomSlug]);
        $messages = $messageRepository->findBy(['room' => $room], ['createdAt' => 'DESC'], 10);

        return $this->render('front/tchat/room.html.twig', [
            'messages' => $messages,
            'room' => $room,
            'lang' => $lang,
        ]);
    }

    // Message ----------------------------------------------------
    #[Route('/room/message/{roomSlug}', name: 'app_tchat_message', methods: ['GET', 'POST'])]
    public function message(
        string $roomSlug,
        RoomRepository  $roomRepository,
        MessageRepository $messageRepository,
        Request $request, 
        SessionInterface $session,
    ): Response
    {
        $lang = $session->get('lang');
        $room = $roomRepository->findOneBy(['slug' => $roomSlug]);
        $messages = $messageRepository->findBy(['room' => $room], ['createdAt' => 'DESC'], 10);
        $message = new Message();
        $message->setRoom($room);
        $message->setSender($this->getUser());
        $message->setCreatedAt(new \DateTime('now'));
        $message->setLastanswer(new \DateTime('now'));

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $content = $form->get('content')->getData();
            $message->setContent($content);
            $messageRepository->save($message, true);

            return $this->redirectToRoute(
                'app_tchat_room',
                 [
                    'roomSlug' => $roomSlug,
                ],
                 Response::HTTP_SEE_OTHER
                );
        }

        return $this->render('front/tchat/message.html.twig', [
            'messages' => $messages,
            'room' => $room,
            'form' => $form->createView(),
            'lang' => $lang,
        ]);
    }

    // Answer ----------------------------------------------------
    #[Route('/room/answer/{roomSlug}/{messageId}', name: 'app_tchat_answer', methods: ['GET', 'POST'])]
    public function answer(
        string $roomSlug,
        int $messageId,
        RoomRepository  $roomRepository,
        MessageRepository $messageRepository,
        Request $request, 
        SessionInterface $session,
    ): Response
    {
        $lang = $session->get('lang');
        $room = $roomRepository->findOneBy(['slug' => $roomSlug]);
        $parent = $messageRepository->findOneBy(['id' => $messageId]);
        $messages = $messageRepository->findBy(['room' => $room], ['createdAt' => 'DESC'], 10);
        $message = new Message();
        $message->setRoom($room);
        $message->setSender($this->getUser());
        $message->setCreatedAt(new \DateTime('now'));
        $message->setLastanswer(new \DateTime('now'));

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $content = $form->get('content')->getData();
            $message->setContent($content);
            $message->setParentMessage($parent);
            $messageRepository->save($message, true);

            return $this->redirectToRoute(
                'app_tchat_room',
                    [
                    'roomSlug' => $roomSlug,
                ],
                    Response::HTTP_SEE_OTHER
                );
        }

        return $this->render('front/tchat/answer.html.twig', [
            'messages' => $messages,
            'room' => $room,
            'form' => $form->createView(),
            'lang' => $lang,
            'parent' => $parent,
        ]);
    }

    #[Route('suppr/{roomSlug}/{messageId}', name: 'app_tchat_suppr', methods: ['POST'])]
    public function suppr(
        string $roomSlug,
        int $messageId,
        Message $message, 
        MessageRepository $messageRepository
        ): Response
    {
        $message = $messageRepository->findOneBy(['id' => $messageId]);
        $messageRepository->remove($message, true);

        return $this->redirectToRoute('app_tchat_index', [
            'roomSlug' => $roomSlug,
        ], Response::HTTP_SEE_OTHER);
    }

    #[Route('/', name: 'app_tchat_index', methods: ['GET'])]
    public function index(MessageRepository $messageRepository): Response
    {
        return $this->render('front/tchat/index.html.twig', [
            'messages' => $messageRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_tchat_new', methods: ['GET', 'POST'])]
    public function new(Request $request, MessageRepository $messageRepository): Response
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $messageRepository->save($message, true);

            return $this->redirectToRoute('app_tchat_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('front/tchat/new.html.twig', [
            'message' => $message,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_tchat_show', methods: ['GET'])]
    public function show(Message $message): Response
    {
        return $this->render('front/tchat/show.html.twig', [
            'message' => $message,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_tchat_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Message $message, MessageRepository $messageRepository): Response
    {
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $messageRepository->save($message, true);

            return $this->redirectToRoute('app_tchat_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('front/tchat/edit.html.twig', [
            'message' => $message,
            'form' => $form,
        ]);
    }

    #[Route('{roomSlug}/{id}', name: 'app_tchat_delete', methods: ['POST'])]
    public function delete(
        string $roomSlug,
        Request $request, Message $message, MessageRepository $messageRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$message->getId(), $request->request->get('_token'))) {
            $messageRepository->remove($message, true);
        }

        return $this->redirectToRoute('app_tchat_room', [
            'roomSlug' => $roomSlug,
        ], Response::HTTP_SEE_OTHER);
    }
}
