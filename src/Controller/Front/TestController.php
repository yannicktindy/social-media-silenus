<?php

namespace App\Controller\Front;

use App\Entity\Impact;
use App\Entity\Score;
use App\Entity\Kinkster;
use App\Form\Type\TestType;
use App\Form\Test2Type;
use App\Repository\ImpactRepository;
use App\Repository\TestRepository;
use App\Repository\FactorRepository;
use App\Repository\KinksterRepository;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class TestController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private TestRepository $testRepository,
        private FactorRepository $factorRepository,
        private ImpactRepository $impactRepository,
        private KinksterRepository $kinksterRepository,
        private ScoreRepository $scoreRepository,
    ) {}

    #[Route('/test/intro/{testSlug}', name: 'app_test')]
    public function index(
        string $testSlug,
        SessionInterface $session,

    ): Response
    {
        $test = $this->testRepository->findOneBy(['testSlug' => $testSlug]);
        $lang = $session->get('lang');

        return $this->render('front/test/intro.html.twig', [
            'controller_name' => 'TestController',
            'test' => $test,
            'testSlug' => $testSlug,
            'lang' => $lang,
        ]);
    }

    #[Route('/test/run/{testSlug}', name: 'app_test_run')]
    public function run(
        string $testSlug,
        RequestStack $requestStack,
        SessionInterface $session,
        EntityManagerInterface $em,
    ): Response
    {
        $lang = $session->get('lang');

        // verifier si le user à déjà fait le test
        if($this->getUser()) {
            $userIdentifier = $this->getUser()->getUserIdentifier();
            $user = $this->kinksterRepository->findOneBy(['email' => $userIdentifier ]);

            $impacts = $this->impactRepository->findBy(['kinkster' => $user]);
            foreach($impacts as $impact) {
                if($impact->getTest()->getTestSlug() == $testSlug) {
                    return $this->redirectToRoute('app_test_done', [
                        'userSlug' => $user->getUserSlug(),
                        'impactSlug' => $impact->getImpactSlug(),
                    ]);
                }
            }
        }
        
        $anonymous = [];
        $test = $this->testRepository->findOneBy(['testSlug' => $testSlug]);
        $quests = $test->getQuestions();
        $arrayQuest = [];
        foreach($quests as $key => $quest) {
            array_push($arrayQuest, $quest);
        }

        $quests = $arrayQuest;
        $step = count($quests);
        
        $factors = $test->getFactors();
        $currentTest = [];
        $questArray = [];
        $countQuests = count($quests);

        $request = $requestStack->getCurrentRequest();

        $testForm = $this->createForm(Test2Type::class, [
            'countQuests' => $countQuests,
        ]);
        $testForm->handleRequest($request);

        if($testForm->isSubmitted() && $testForm->isValid()) {      
            $test->setCount($test->getCount() + 1);  
            $em->persist($test);
            $em->flush();
            $data = $testForm->getData();

            for ($i = 0; $i < $data['countQuests']; $i++) {
                array_push($questArray, [
                    'factor' => $quests[$i]->getFactor(),
                    'force' => $data['choices'.$i],
                ]); 
            }
            
            foreach($factors as $factor) {
                $sum = 0;
                foreach($questArray as $quest) {
                    if($factor == $quest['factor']) {
                        $sum = $sum + $quest['force'];
                    }                
                }
                array_push($currentTest, [
                    'force' => $sum,
                    'factor' => $factor,
                    
                ]);
            }

            // if user or is anonymous
            if($this->getUser()) {
                $userIdentifier = $this->getUser()->getUserIdentifier();
                $user = $this->kinksterRepository->findOneBy(['email' => $userIdentifier ]);

                // create impact
                $userSlug= $user->getUserSlug();

                $impact = new Impact();
                $impact->setCreation(new \DateTime('now'));
                $impactSlug = 'impact-' . $test->getTestSlug() . '-' . $userSlug . '-' . uniqid();
                $impact->setImpactSlug($impactSlug);
                $impact->setTest($this->testRepository->findOneBy(['testSlug' => $testSlug]));
                $impact->setKinkster($user);
                $impact->setGenLevel(0);
    
                $em->persist($impact);
                
                foreach($currentTest as $result) {
                    $score = new Score();
                    $score->setLevel($result['force']);
                    $score->setFactor($result['factor']);
                    $score->setImpact($impact);
                    $em->persist($score);
                }
    
                $em->flush();
    
                return $this->redirectToRoute('app_test_validation', ['impactSlug' => $impactSlug , 'userSlug' => $userSlug]);

            } else {
                // $user = $this->kinksterRepository->findOneBy(['id' => 1 ]);
                arsort($currentTest);
                array_push($anonymous, [
                    'test' => $test,
                    'scores' => $currentTest,
                ]);
                $session->set('anonymous', $anonymous); 

                return $this->redirectToRoute('app_test_anonymous');

            }
            


        }

        return $this->render('front/test/run.html.twig', [
            'controller_name' => 'TestController',
            'testForm' => $testForm->createView(),
            'step' => $step,
            'test' => $test,
            'quests' => $quests,
            'countQuests' => $countQuests,
            'lang' => $lang,
            // 'currentQuest' => $currentQuest,
        ]);
    }

    #[Route('/test/validation/{impactSlug}/{userSlug}', name: 'app_test_validation')]
    public function valitation(
        string $impactSlug,
        string $userSlug,
        SessionInterface $session,
        EntityManagerInterface $entityManagerInterface,
        KinksterRepository $kinksterRepository,

        // UserInterface $user,
    ) : Response  
    {
        $lang = $session->get('lang');
        $impact = $this->impactRepository->findOneBy(['impactSlug' => $impactSlug]);
        $user = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $test = $impact->getTest();
        $testSlug = $test->getTestSlug();

        return $this->render('front/test/validation.html.twig', [
            'controller_name' => 'TestController',
            'impact' => $impact,
            'user' => $user,
            'test' => $test,
            'lang' => $lang,
        ]);
    } 

    #[Route('/test/result/{impactSlug}', name: 'app_test_result')]
    public function result(
        string $impactSlug,
        SessionInterface $session,
        // string $testSlug,
        // string $userSlug,   
    ): Response
    {
        $lang = $session->get('lang');
        $impact = $this->impactRepository->findOneBy(['impactSlug' => $impactSlug]);
        $scores = $this->scoreRepository->findBy(['impact' => $impact]);
        $kinkster = $impact->getKinkster();
        $test = $impact->getTest();
        $resultArray = [];
        
        foreach ($scores as $score) {
            $factor = $score->getFactor();
            array_push($resultArray, [
                'level' => $score->getLevel(),
                'factor' => $factor,
            ]);
        }

        arsort($resultArray);
        return $this->render('front/test/result.html.twig', [
            'controller_name' => 'TestController',
            'test' => $test,
            'kinkster' => $kinkster,
            'resultArray' => $resultArray,
            'lang' => $lang,
        ]);
    }

    #[Route('/test/done/{userSlug}/{impactSlug}', name: 'app_test_done')]
    public function done(
        string $impactSlug,
        string $userSlug,   
        SessionInterface $session,
    ): Response
    {
        $lang = $session->get('lang');
        $kinkster = $this->kinksterRepository->findOneBy(['userSlug' => $userSlug]);

        $impact = $this->impactRepository->findOneBy(['impactSlug' => $impactSlug]);
        $test = $impact->getTest();

        return $this->render('front/test/done.html.twig', [
            'controller_name' => 'TestController',
            'kinkster' => $kinkster,
            'impact' => $impact,
            'test' => $test,
            'lang' => $lang,
        ]);
    }

    #[Route('/test/anonymous', name: 'app_test_anonymous')]
    public function anonymous(
        SessionInterface $session,
        // string $testSlug,
        // string $userSlug,   
    ): Response
    {
        $lang = $session->get('lang');
        $anonymous = $session->get('anonymous');
        // dd($anonymous);
        // $impact = $this->impactRepository->findOneBy(['impactSlug' => $impactSlug]);
        // $scores = $this->scoreRepository->findBy(['impact' => $impact]);
        // $kinkster = $impact->getKinkster();
        // $test = $impact->getTest();
        // $resultArray = [];
        
        // foreach ($scores as $score) {
        //     $factor = $score->getFactor();
        //     array_push($resultArray, [
        //         'level' => $score->getLevel(),
        //         'name' => $factor->getNom(),
        //         'description' => $factor->getDescFr(),
        //     ]);
        // }

        // arsort($resultArray);

        return $this->render('front/test/anonyme.html.twig', [
            'controller_name' => 'TestController',
            'anonymous' => $anonymous,
            'lang' => $lang,
            // 'test' => $test,
            // 'kinkster' => $kinkster,
            // 'resultArray' => $resultArray,
        ]);
    }    

    #[Route('/test/list/', name: 'app_test_list')]
    public function list(
        SessionInterface $session, 

    ): Response
    {
        $lang = $session->get('lang');
        $basics = $this->testRepository->findBy(['advanced' => false]);
        $advanced = $this->testRepository->findBy(['advanced' => true]);

        return $this->render('front/test/list.html.twig', [
            'controller_name' => 'TestController',
            'basicTests' => $basics,
            'advancedTests' => $advanced,
            'lang' => $lang,
        ]);
    }
}
