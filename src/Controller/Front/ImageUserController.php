<?php

namespace App\Controller\Front;

use App\Entity\ImageUser;
use App\Entity\Kinkster;
use App\Form\ImageUserType;
use App\Repository\ImageUserRepository;
use App\Repository\KinksterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


#[Route('/image/user')]
class ImageUserController extends AbstractController
{

    #[Route('/{userSlug}', name: 'app_image_user_index', methods: ['GET'])]
    public function index(
        string $userSlug,
        ImageUserRepository $imageUserRepository,
        KinksterRepository $kinksterRepository,
        ): Response
    {

        $kinkster = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        $countImage = count($kinkster->getImageUsers());
        $profilImage= '';
        $idImage = 0;
        if($countImage > 0){
            $profilImage= $kinkster->getImageUsers()[0]->getName();
            $idImage = $kinkster->getImageUsers()[0]->getId();
        }
    
        return $this->render('front/image_user/gallery.html.twig', [
            'countImage' => $countImage,
            'kinkster' => $kinkster,
            'profilImage' => $profilImage,
            'idImage' => $idImage,
        ]);

    }

    #[Route('/new/{userSlug}', name: 'app_image_user_new', methods: ['GET', 'POST'])]
    public function new(
        string $userSlug,
        Request $request, 
        ImageUserRepository $imageUserRepository,
        KinksterRepository $kinksterRepository
        ): Response
    {
        $kinkster = $kinksterRepository->findOneBy(['userSlug' => $userSlug]);
        
        $imageUser = new ImageUser();
        $imageUser->setUser($kinkster);
        $imageUser->setIsValid(true);
        $imageUser->setIsFirst(true);
        $form = $this->createForm(ImageUserType::class, $imageUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $brochureFile */
            // recupere le fichier image du formulaire 'image' 
            //ce n'est pas le nom de la propriété name de l'entité
            $imageFile = $form->get('image')->getData();
            $pseudo = $kinkster->getPseudo();
            // this condition is needed because the 'image' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);

                // this is needed to safely include the file name as part of the URL
                // le nom du fichier commence par le pseudo de l'utilisateur
                $safeFilename = $pseudo;
                $cropFilename= $safeFilename.'-crop-'.uniqid().'.'.$imageFile->guessExtension();
                $resizeFilename = $safeFilename.'-resize-'.uniqid().'.'.$imageFile->guessExtension();
                $tmpFilename = $safeFilename.'-tmp-'.uniqid().'.'.$imageFile->guessExtension();
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                //---------- sort extension ----------------
                $extension = $imageFile->guessExtension();
                if($extension=="jpg" || $extension=="jpeg" ){ 
                $source = imagecreatefromjpeg($imageFile);
                }
                
                else if($extension=="png"){
                // $uploadedfile = $_FILES['image']['tmp_name'];
                $source = imagecreatefrompng($imageFile);
                }
                
                else {
                $source = imagecreatefromgif($imageFile);
                }

                //---------- crop square and resize image before storage ----------------
                // ------------ to do : make a service for that
                $size = getimagesize($imageFile);
                $width = $size[0];
                $height = $size[1];

                
                if ($width > $height) {
                    $y = 0;
                    $x = ($width - $height) / 2;
                    $smallestSide = $height;
                } elseif ($height > $width) {
                    $x = 0;
                    $y = ($height - $width) / 2;
                    $smallestSide = $width;
                } else {
                    $x = 0;
                    $y = 0;
                    $smallestSide = $width;
                }
                $thumbSize = 300;
                $crop = imagecreatetruecolor($thumbSize, $thumbSize);
                imagecopyresampled($crop, $source, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);
                imagepng($crop, $this->getParameter('imageuser_directory').'/'.$cropFilename, 9);
                
                $imageUser->setName($cropFilename);
            }
            // persite l'entité
            $imageUserRepository->save($imageUser, true);

            return $this->redirectToRoute('app_image_user_index', ['userSlug' => $userSlug], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('front/image_user/new.html.twig', [
            'image_user' => $imageUser,
            'form' => $form,
        ]);
    }


    #[Route('/{id}', name: 'app_image_user_show', methods: ['GET'])]
    public function show(ImageUser $imageUser): Response
    {
        return $this->render('front/image_user/show.html.twig', [
            'image_user' => $imageUser,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_image_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ImageUser $imageUser, ImageUserRepository $imageUserRepository): Response
    {
        $form = $this->createForm(ImageUserType::class, $imageUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageUserRepository->save($imageUser, true);

            return $this->redirectToRoute('app_image_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('front/image_user/edit.html.twig', [
            'image_user' => $imageUser,
            'form' => $form,
        ]);
    }

    #[Route('/{userSlug}/{id}', name: 'app_image_user_delete', methods: ['POST'])]
    public function delete(string $userSlug, Request $request, ImageUser $imageUser, ImageUserRepository $imageUserRepository, ): Response
    {
        if ($this->isCsrfTokenValid('delete'.$imageUser->getId(), $request->request->get('_token'))) {
            // supprime le fichier image
            unlink($this->getParameter('imageuser_directory').'/'.$imageUser->getName());
            // supprime l'instance de l'entité
            $imageUserRepository->remove($imageUser, true);
            
        }

        return $this->redirectToRoute('app_image_user_index', [
            'userSlug' => $userSlug
        ], Response::HTTP_SEE_OTHER);
    }
}
