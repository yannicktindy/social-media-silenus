<?php

namespace App\Controller\Front;

use App\Entity\Visit;
use App\Form\StartType;
use App\Repository\VisitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class StartController extends AbstractController
{
    #[Route('/', name: 'app_start_enter')]
    public function enter(
        RequestStack $requestStack,
        SessionInterface $session 
    ): Response
    {

        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }
       
        
        $session->set('validation', false);

        return $this->render('front/start/enter.html.twig', [
            'controller_name' => 'StartController',
            
        ]);
    }

    #[Route('/fr', name: 'app_start_fr')]
    public function fr(
        RequestStack $requestStack,
        SessionInterface $session,
        VisitRepository $visitRepository, 
    ): Response
    {
        $lang = 'fr';
        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }
        $session->set('lang', $lang);

        $request = $requestStack->getCurrentRequest();
        $startForm = $this->createForm(StartType::class);
        $startForm->handleRequest($request);

        if ($startForm->isSubmitted() && $startForm->isValid()) {
            $session->set('validation', true);
            $newVisit = new Visit();
            $newVisit->setVisitDate(new \DateTime('now'));
            // persit new visit
            $visitRepository->save($newVisit, true);
            $validation = $session->get('validation');

            $session->set('validation', true);

            return $this->redirectToRoute('app_home');
        }        

        return $this->render('front/start/choice.html.twig', [
            'controller_name' => 'StartController',
            'startForm' => $startForm->createView(),
            'lang' => 'fr',
            
        ]);
    }

    #[Route('/eng', name: 'app_start_eng')]
    public function eng(
        RequestStack $requestStack,
        SessionInterface $session,
        VisitRepository $visitRepository, 
    ): Response
    {
        $lang = 'eng';
        $validation = $session->get('validation');
        if ($validation == true || $this->getUser()) {

            return $this->redirectToRoute('app_home');
        }
        $session->set('lang', $lang);
        
        $request = $requestStack->getCurrentRequest();
        $startForm = $this->createForm(StartType::class);
        $startForm->handleRequest($request);

        if ($startForm->isSubmitted() && $startForm->isValid()) {
            $session->set('validation', true);
            $newVisit = new Visit();
            $newVisit->setVisitDate(new \DateTime('now'));
            // persit new visit
            $visitRepository->save($newVisit, true);
            $validation = $session->get('validation');

            $session->set('validation', true);

            return $this->redirectToRoute('app_home');
        }        

        return $this->render('front/start/choice.html.twig', [
            'controller_name' => 'StartController',
            'startForm' => $startForm->createView(),
            'lang' => 'eng',
        ]);
    }





    #[Route('/validation', name: 'app_start_validation')]
    public function validation(
        RequestStack $requestStack,
        SessionInterface $session,
        VisitRepository $visitRepository, 
    ): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        $lang = $session->get('lang');

        $request = $requestStack->getCurrentRequest();
        $startForm = $this->createForm(StartType::class);
        $startForm->handleRequest($request);

        if ($startForm->isSubmitted() && $startForm->isValid()) {
            $session->set('validation', true);
            $newVisit = new Visit();
            $newVisit->setVisitDate(new \DateTime('now'));
            // persit new visit
            $visitRepository->save($newVisit, true);
            $validation = $session->get('validation');

            $session->set('validation', true);

            return $this->redirectToRoute('app_home');
        }        

        return $this->render('front/start/validation.html.twig', [
            'controller_name' => 'StartController',
            'startForm' => $startForm->createView(),
            'lang' => $lang,
        ]);
    }

    // #[Route('/', name: 'app_start')]
    // public function start(
    //     RequestStack $requestStack,
    //     SessionInterface $session 
    // ): Response
    // {
    //     if ($this->getUser()) {
    //         return $this->redirectToRoute('app_home');
    //     }

    //     return $this->render('front/start/lang.html.twig', [
    //         'controller_name' => 'StartController',
            
    //     ]);
    // }

    // #[Route('/start', name: 'app_start_choice')]
    // public function choice(
    //     RequestStack $requestStack,
    //     SessionInterface $session,
    //     VisitRepository $visitRepository,
    // ): Response
    // {
        
    //     $request = $requestStack->getCurrentRequest();
    //     $startForm = $this->createForm(StartType::class);
    //     $startForm->handleRequest($request);

    //     if ($startForm->isSubmitted() && $startForm->isValid()) {
    //         $session->set('validation', true);
    //         $newVisit = new Visit();
    //         $newVisit->setVisitDate(new \DateTime('now'));
    //         // persit new visit
    //         $visitRepository->save($newVisit, true);

    //         return $this->redirectToRoute('app_home');
    //     }

    //     return $this->render('front/start/choice.html.twig', [
    //         'controller_name' => 'StartController',
    //         'startForm' => $startForm->createView(),
    //     ]);
    // }

    // #[Route('/start/tryone', name: 'app_start_tryone')]
    // public function tryone(

    // ): Response
    // {


    //     return $this->render('front/start/tryone.html.twig', [
    //         'controller_name' => 'StartController',
            
    //     ]);
    // }
}
