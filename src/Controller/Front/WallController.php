<?php

namespace App\Controller\Front;

use App\Repository\ImpactRepository;
use App\Repository\KinksterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class WallController extends AbstractController
{
    public function __construct(
        private ImpactRepository $impactRepository,
        private KinksterRepository $kinksterRepository,
         
    ) {}

    #[Route('/wall', name: 'app_wall')]
    public function index(SessionInterface $session,): Response
    {
        $lang = $session->get('lang');
        $impacts = $this->impactRepository->findBy([], ['creation' => 'DESC'], 10);
        $profils = $this->kinksterRepository->findBy([], ['creation' => 'DESC'], 20);
        return $this->render('front/wall/index.html.twig', [
            'controller_name' => 'WallController',
            'profils' => $profils,
            'impacts' => $impacts,
            'lang' => $lang,
        ]);
    }
}
