<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerService
{
    public function __construct(
        private MailerInterface $mailer,

        )
    {
    }
    public function registrationEmail(
        string $to, 
        string $subject,
        string $userName,
        string $userSlug,
        ): void
    {

        $email = (new TemplatedEmail())
            ->from('him@insidiome.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate('front/email/registration.html.twig')
            ->context([
                'userName' => $userName,
                'userSlug' => $userSlug,
            ])
            ;
        $this->mailer->send($email);
    }

}    